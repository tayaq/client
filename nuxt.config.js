import axios from 'axios'

export default {
    // Global page headers (https://go.nuxtjs.dev/config-head)

    globalName: 'yimyim',
    cache: true,

    head: {
        htmlAttrs: {
            lang: 'ru',
        },
        title: 'Доставка еды от Yim-Yim заказать на софиевскую и петропавловскую борщаговку',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { name: 'facebook-domain-verification', content: 'fo1def12w82ilc2adl1v2togn0v8o4' },
            {
                hid: 'description',
                name: 'description',
                content: '⭐️Заказать еду с доставкой 🍴 от ►Yimyim◄ ✔️Быстро ✔️Качественно ✔️ Демократично⭐ Большие порции 😋 Заказ онлайн или по телефону с доставкой на софиевской и петропавловской борщаговке без выходных 📞 +38(098)877-08-80 и Софиевская Борщаговка, ЖК Петровский Квартал, бул. Леси Украинки 12 💳 Оплата картой'
            }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [
        '@/assets/styles/main.sass'

    ],
    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [
        {
            src: '~plugins/axios.js'
        },
        {
            src: '~/plugins/agile.js',
            mode: 'client'
        },
        {
            src: '~/plugins/outside.js',
            mode: 'client'
        },
        {
            src: '~/plugins/persistedstate.js',
        },
        {
            src: '~/plugins/phone-mask.js',
            mode: 'client'
        }
    ],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: [
        '~/components/sections',
        '~/components/pages',
        '~/components/news',
        '~/components/cart',
        '~/components/products',
        '~/components/modal/templates',
        '~/components/'
    ],

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/typescript
        // '@nuxt/typescript-build'
        '@nuxt/components',
        '@nuxtjs/dotenv',
        '@nuxtjs/google-fonts'
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/svg-sprite',
        '@nuxtjs/style-resources',
        '@nuxtjs/component-cache',
        '@nuxtjs/sitemap',
        '@nuxtjs/dayjs',
        '@nuxtjs/google-gtag',
        '@nuxtjs/gtm',
        // 'nuxt-lazy-load'
    ],

    'google-gtag': {
        id: 'G-QR1B9Y9ZQC'
    },

    gtm: {
        id: 'GTM-5F6RKV2'
    },

    googleFonts: {
        families: {
            Nunito: [300, 400, 600, 700, 800, 900]
        },
        display: 'swap'
    },

    sitemap: {
        hostname: 'https://yimyim.com.ua',
        gzip: true,
        exclude: [
            '/_icons'
        ],
        cacheTime: 1000 * 60 * 60 * 24,
        defaults: {
            priority: 1,
            lastmod: new Date()
        },
        routes: async () => {
            const categories = await axios.get(`${process.env.BASE_URL}/categories/`)
            const products = await axios.get(`${process.env.BASE_URL}/products/`)
            return [
                ...categories.data.map((category) => `/${category.url}`),
                ...products.data.map(product => `/${product.category.url}/${product.url}`)
            ]
        }
    },

    styleResources: {
        sass: ['@/assets/styles/variables.sass', '@/assets/styles/mixins.sass']
    },

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {
        // baseURL: 'http://31.131.24.65/api'
        baseURL: process.env.BASE_URL
    },

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {
        transpile: ['vue-agile'],
        publicPath: '/dist/',
        // splitChunks: {
        //     layouts: true,
        //     pages: true,
        //     commons: true
        // },
        // optimization: {
        //     splitChunks: {
        //         cacheGroups: {
        //             styles: {
        //                 name: 'styles',
        //                 test: /\.(css|vue)$/,
        //                 chunks: 'all',
        //                 enforce: true
        //             }
        //         }
        //     }
        // }
    },
}

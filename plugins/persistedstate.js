import createPersistedState from 'vuex-persistedstate';
import * as Cookies from 'js-cookie';
import cookie from 'cookie';

export default ({ store, req }) => {
    createPersistedState({
        key: 'cart',
        paths: ['cart.list', 'cart.order'],
        storage: {
            getItem: (key) => {
                if (process.server && req.headers.cookie) {
                    const parsedCookies = cookie.parse(req.headers.cookie);
                    return parsedCookies[key];
                } else {
                    return Cookies.get(key);
                }
            },
            setItem: (key, value) =>
                Cookies.set(key, value, { expires: 7, secure: false }),
            removeItem: key => Cookies.remove(key)
        }
    })(store);
};

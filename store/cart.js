import { getField, updateField } from 'vuex-map-fields';

export const state = () => ({
    list: [],
    order: {
        name: '',
        phone: '',
        callback: true,
        address: {
            street: '',
            house: '',
            entrance: '',
            floor: '',
            flat: ''
        },
        payment: '',
        persons: 1,
        comment: ''
    }
})

export const mutations = {
    addProduct(state, data) {
        let { sizes, hasAdditional } = findProductInCart(state, data);
        if (sizes.length === 0 || sizes.length > 0 && !hasAdditional) state.list.push({ ...data, qty: 1 })
    },
    removeProduct(state, data) {
        state.list = state.list.filter(size => size !== data);
    },
    clearCart(state) {
        state.list = [];
    },
    changeProductQTY(state, { action, data, value }) {
        let { findSize } = findProductInCart(state, data);
        switch (action) {
            case 'set':
                if (value >= 1) findSize.qty = value;
                else findSize.qty = 1;
                break;
            case 'increment':
                findSize.qty++;
                break;
            case 'decrement':
                if (findSize.qty > 1) findSize.qty--;
                else state.list = state.list.filter(size => size !== data);
                break;
        }
    },
    updateField
}

export const actions = {
    addProduct({ commit }, product) {
        commit('addProduct', product)
    },
    removeProduct({ commit }, id) {
        commit('removeProduct', id)
    },
}

export const getters = {
    getList: state => {
        return state.list
    },
    order: state => {
        return state.order
    },
    getField
}

function findProductInCart(state, data) {
    let sizes = state.list.filter(item => data.size.id === item.size.id && data.additionals.length === item.additionals.length)
    let findSize;
    let hasAdditional = sizes.some(size => {
        findSize = {};
        if (data.additionals.length === 0) {
            let length = size.additionals.length === 0;
            if (length) findSize = size;
            return length;
        }
        let every = data.additionals.every(additional => {
            return size.additionals.includes(additional);
        });
        if (every) findSize = size;
        return every;
    });
    return { sizes, hasAdditional, findSize }
}

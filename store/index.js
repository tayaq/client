export const state = () => ({
    headerDropdownShow: '',
    categories: [],
    products: [],
    pages: [],
    news: []
})

export const mutations = {
    setCategories (state, categories) {
        state.categories = categories;
    },
    setProducts (state, products) {
        state.products = products;
    },
    setPages (state, pages) {
        state.pages = pages;
    },
    setNews (state, news) {
        state.news = news;
    },
    setHeaderDropdownShow (state, value) {
        state.headerDropdownShow = value;
    }
}

export const actions = {

    async fetchCategories ({ commit }) {
        const categories = await this.$axios.$get('categories');
        await commit('setCategories', categories);
    },

    async fetchPages ({ commit }) {
        const pages = await this.$axios.$get('pages');
        await commit('setPages', pages);
    },

    async fetchProducts ({ commit }) {
        const categories = await this.$axios.$get('products');
        await commit('setProducts', categories);
    },

    async nuxtServerInit ({ dispatch }) {
        await dispatch('fetchCategories');
        await dispatch('fetchProducts');
        await dispatch('fetchPages');
    }

}

export const getters = {
    categories: state => state.categories,
    products: state => state.products,
    pages: state => state.pages,
    news: state => state.news,
    headerDropdownShow: state => state.headerDropdownShow
}

export const state = () => ({
    config: {
        template: '',
        closeVisible: true,
        position: 'top',
        className: 'default'
    },
    open: false
})

export const mutations = {
    setTemplate(state, template) {
        state.template = template;
    },
    openModal(state, config) {
        for (let value in config) {
            if (config.hasOwnProperty(value)) state.config[value] = config[value];
        }
        state.open = true;
    },
    closeModal(state) {
        state.config.template = '';
        state.open = false;
    },
}

// export const actions = {
//
//     openModal({commit}, template) {
//         commit('setTemplate', template);
//         commit('setShow', true);
//     },
//
//     closeModal({commit}) {
//         commit('setShow', false);
//     },
//
// }

export const getters = {
    config: state => state.config,
    open: state => state.open
}
